## Ejercicio - Anagrams

Define los métodos `is_anagram?` y `anagrams` que tienen la función de encontrar palabras que pertenecen al mismo grupo de palabras que son anagramas y entregar estos grupos de palabras en un arreglo. 

Recuerda que los anagramas son palabras que tienen las mismas letras pero en orden diferente.

>Restricción: Dividir responsabilidades ([Single Responsibility Principle](https://thoughtbot.com/blog/back-to-basics-solid)).


```ruby
#anagrams method



#is_anagram? method




#Driver code

words =  ['demo', 'none', 'tied', 'evil', 'dome', 'mode', 'live',
          'fowl', 'veil', 'wolf', 'diet', 'vile', 'edit', 'tide',
          'flow', 'neon']

p anagrams(words) == [["demo", "dome", "mode"], ["none", "neon"], ["tied", "diet", "edit", "tide"], ["evil", "live", "veil", "vile"], ["fowl", "wolf", "flow"]]
```